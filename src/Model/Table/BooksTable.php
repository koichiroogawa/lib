<?php
namespace App\Model\Table;
 
use Cake\ORM\Table;
 
class BooksTable extends Table
{
 public function initialize(array $config)
 {
 	$this->belongsTo('Users');
 }
}