<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">
  </head>
  <body>
    <table>
    <tr>
        <th>ID</th>
        <th>NAME</th>
        <th>ADDRESS</th>
        <th>AGE</th>
        <!-- <th>COMPANY_ID</th> -->
    </tr>
    <?php foreach ($books as $book): ?>
    <tr>
        <td><?= $this->Number->format($book->id) ?></td>
        <td><?= h($book->name) ?></td>
        <td><?= h($book->users->name) ?></td>
    </tr>
    <?php endforeach; ?>
    </table>
    <div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<<') ?>
        <?= $this->Paginator->prev('<') ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next('>') ?>
        <?= $this->Paginator->last('>>') ?>
    </ul>
</div>

  </body>
</html>