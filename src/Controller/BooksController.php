<?php
namespace App\Controller;
 
use App\Controller\AppController;
 
class BooksController extends AppController
{
 public $paginate = [
 'limit' => 4 // 1ページに表示するデータ件数
 // 'contain' => ['Users'] // Companyテーブルを結合する

 ];
 
 public function index()
 {
 $this->set('books', $this->paginate());
 }
}