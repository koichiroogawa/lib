<?php
class ArticlesController extends AppController {
 
    public $paginate = [
        'limit' => 25,
        'order' => [
            'Articles.title' => 'asc'
        ]
    ];
 
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
}